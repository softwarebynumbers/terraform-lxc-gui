terraform {
  required_providers {
    lxd = {
      source = "terraform-lxd/lxd"
      version = ">= 1.7.1"
    }
  }
}

variable "host-xid" {
  description = "The host X ID, ie echo $DISPLAY  :1 => X1"
  default = "X1"
}

resource "lxd_profile" "gui_profile" {

  name = "gui_profile"
  description = "GUI profile ui based tools"

  config = {
    "environment.DISPLAY" = ":0"
    "environment.PULSE_SERVER" = "unix:/home/ubuntu/pulse-native"
    "environment.DESKTOP_SESSION" = "ubuntu"
    "nvidia.driver.capabilities" = "all"
    "nvidia.runtime" = "true"
    #cloud-config
    "user.user-data" = "runcmd: \n  'sed -i \"s/; enable-shm = yes/enable-shm = no/g\" /etc/pulse/client.conf'\n  packages: \n  x11-apps \n  mesa-utils \n  pulseaudio"
  } 

  device  {
    name = "PASocket"
    type = "disk" 
    properties = {
      path = "/tmp/.pulse-native"
      source = "/run/user/1000/pulse/native"
    }
  }


  device {
    name = "X0"
    type = "proxy"
    properties = {
      bind = "container"
      connect = "unix:@/tmp/.X11-unix/${var.host-xid}"
      listen  = "unix:@/tmp/.X11-unix/X0"
      "security.gid" = "1000"
      "security.uid" = "1000"
    }
  }

  device {
    name = "mygpu"
    type = "gpu" 
    properties = {
    }
  }

}

